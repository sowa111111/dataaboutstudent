﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAboutStudent
{
    public class Student
    {
        private string fio;
        private byte age;
        private byte gender;
        private byte classStudent;
        private double avgMark;

        public Student(string FIO, byte classStudent, byte age, byte gender,  double avgMark)
        {
            this.fio = FIO;
            this.age = age;
            this.gender = gender;
            this.classStudent = classStudent;
            this.avgMark = avgMark;
        }

        public Student()
        { }

        public string FIO
        {
            get
            {
                return this.fio;
            }
            set 
            {
                this.fio = value;
            }
        }

        public byte Age
        { 
            get { return this.age;}
            set { this.age = value; }
        }

        public byte Gender
        {
            get { return this.gender; }
            set { this.gender = value; }
        }

        public byte ClassStudent
        {
            get { return this.classStudent; }
            set { this.classStudent = value; }
        }

        public double AvgMark
        {
            get { return this.avgMark; }
            set { this.avgMark = value; }
        }

    }
}
