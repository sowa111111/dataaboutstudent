﻿namespace DataAboutStudent
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.aboutProgramme = new System.Windows.Forms.Label();
            this.btn_cancle = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // aboutProgramme
            // 
            this.aboutProgramme.AutoSize = true;
            this.aboutProgramme.Font = new System.Drawing.Font("a_BosaNova", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.aboutProgramme.Location = new System.Drawing.Point(12, 9);
            this.aboutProgramme.Name = "aboutProgramme";
            this.aboutProgramme.Size = new System.Drawing.Size(53, 16);
            this.aboutProgramme.TabIndex = 0;
            this.aboutProgramme.Text = "label1";
            // 
            // btn_cancle
            // 
            this.btn_cancle.Location = new System.Drawing.Point(281, 290);
            this.btn_cancle.Name = "btn_cancle";
            this.btn_cancle.Size = new System.Drawing.Size(75, 23);
            this.btn_cancle.TabIndex = 12;
            this.btn_cancle.Text = "Отмена";
            this.btn_cancle.UseVisualStyleBackColor = true;
            this.btn_cancle.Click += new System.EventHandler(this.btn_cancle_Click);
            // 
            // About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 336);
            this.ControlBox = false;
            this.Controls.Add(this.btn_cancle);
            this.Controls.Add(this.aboutProgramme);
            this.Name = "About";
            this.Text = "About";
            this.Load += new System.EventHandler(this.About_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label aboutProgramme;
        private System.Windows.Forms.Button btn_cancle;
    }
}