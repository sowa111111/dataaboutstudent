﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataAboutStudent
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        public string FILE_NAME = "about.txt";


        private void About_Load(object sender, EventArgs e)
        {
            try
            {

                StreamReader fst = new StreamReader( FILE_NAME, Encoding.GetEncoding(1251));
                this.aboutProgramme.Text = "";
                string input = null;

                while ((input = fst.ReadLine()) != null)
                {
                    this.aboutProgramme.Text = this.aboutProgramme.Text + input + "\n";

                }
                fst.Close();
            }

            catch (IOException err)
            {
                Console.WriteLine(err.Message + "\n Ошибка при открытии входного файла ");
                return;
            }
        }

        private void btn_cancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
