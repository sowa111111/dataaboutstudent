﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataAboutStudent
{
    public partial class AddData : Form
    {
        public AddData()
        {
            InitializeComponent();
            gender.SelectedIndex = 0;
        }

        private void btBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private const string FILE_NAME = "DataAboutStudent.txt";
        
        private void button1_Click(object sender, EventArgs e)
        {
            if (FIO.Text == "" || avgMark.Text == "" || classStudent.Text == ""
                || age.Text == "")
            {
                MessageBox.Show("Введенны не все данные", "Ошибка!", MessageBoxButtons.OK);
            }
            else
            {
                Student s = Validation();
                if (s == null)
                {
                    MessageBox.Show("Введенны некорректные данные!", "Ошибка!", MessageBoxButtons.OK);
                }
                else 
                {
                    BinaryWriter fin;

                    try
                    {
                        fin = new BinaryWriter(new FileStream(FILE_NAME, FileMode.Append));
                    }
                    catch (IOException err)
                    {
                        Console.WriteLine(err.Message + "\n Ошибка при открытии файла! "); 
                        return; 
                    }

                    try
                    {
                        fin.Write(s.FIO);
                        fin.Write(s.ClassStudent);
                        fin.Write(s.Age);
                        fin.Write(s.Gender);
                        fin.Write(s.AvgMark);
                    }
                     catch (IOException err) 
                    { 
                        Console.WriteLine(err.Message + "\n Ошибка записи в файл!"); 
                         return;
                    }

                    fin.Close();
                    this.Controls.OfType<TextBox>().All(x => (x.Text = string.Empty) == string.Empty);

                    MessageBox.Show("Все ок","Новые данные",MessageBoxButtons.OK);
                }

                
            }
        }

        private Student Validation()
        {
            bool flag = true;
            Student n = new Student();

            try
            {
                n.FIO = FIO.Text;
                n.Age = Convert.ToByte(age.Text);
                n.AvgMark = Convert.ToDouble(avgMark.Text);
                n.ClassStudent = Convert.ToByte(classStudent.Text);
                n.Gender = (byte)gender.SelectedIndex;
            }
            catch
            {
                flag = false;
            }

            return flag?n:null;
        }


    }
}
