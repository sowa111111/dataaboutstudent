﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataAboutStudent
{
    public partial class Request : Form
    {
        public Request()
        {
            InitializeComponent();
            classStudent.SelectedIndex = 0;
        }

        private void btn_cancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private const string FILE_NAME = "DataAboutStudent.txt";
        private const string FILE_NAME2 = "RequestAboutStudent.txt";

        private void btn_Ok_Click(object sender, EventArgs e)
        {
            List<Student> students = new List<Student>();
            double avgMarkAllStudents = 0;

            BinaryReader fout;
            try
            {
                fout = new BinaryReader(new FileStream(FILE_NAME, FileMode.Open));
            }
            catch (IOException err)
            {
                Console.WriteLine(err.Message + "\n Ошибка при открытии файла! ");
                return;
            }

            while (fout.BaseStream.Position != fout.BaseStream.Length)
            {
                Student s;

                try
                {
                    s = new Student(fout.ReadString(), fout.ReadByte(), fout.ReadByte(), fout.ReadByte(), fout.ReadDouble());

                    if (s.ClassStudent == classStudent.SelectedIndex + 1)
                    {
                        students.Add(s);
                        avgMarkAllStudents += s.AvgMark;
                    }
                }
                catch (IOException err)
                {
                    Console.WriteLine(err.Message);
                    Console.WriteLine("Ошибка чтения файла!");
                    return;
                }

            }

            fout.Close();

            BinaryWriter fin;

            try
            {
                fin = new BinaryWriter(new FileStream(FILE_NAME2, FileMode.Create));
            }
            catch (IOException err)
            {
                Console.WriteLine(err.Message + "\n Ошибка при открытии файла! ");
                return;
            }

            if (students.Count == 0)
            {
                MessageBox.Show("Данных студентах " + classStudent.Text + " курса нет!","Внимание",MessageBoxButtons.OK);
                return;
            }

            double mark = avgMarkAllStudents/students.Count();

            for (int i = 0; i < students.Count; i++)
            {
                if (students[i].AvgMark > mark)
                {
                    try
                    {
                        fin.Write(students[i].FIO);
                        fin.Write(students[i].ClassStudent);
                        fin.Write(students[i].Age);
                        fin.Write(students[i].Gender);
                        fin.Write(students[i].AvgMark);
                    }
                    catch (IOException err)
                    {
                        Console.WriteLine(err.Message + "\n Ошибка записи в файл!");
                        return;
                    }
                }
            }
            fin.Close();
            MessageBox.Show("Запрос выполне!","Ура",MessageBoxButtons.OK);
            this.Close();
        }
    }
}
