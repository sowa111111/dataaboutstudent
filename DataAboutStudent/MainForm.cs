﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace DataAboutStudent
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            load_data(FILE_NAME);
        }

        private const string FILE_NAME = "DataAboutStudent.txt";
        private const string FILE_NAME2 = "RequestAboutStudent.txt";
        int i = 0;

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Interval = 300;
            string my_str = "Данные о студентах";
            this.Text = my_str.Substring(0, i);
            i++;
            if (i > my_str.Length)
            {
                i = 0;
            }

        }

        private void btExit_Click(object sender, EventArgs e)
        {
            DialogResult rez = MessageBox.Show("Хотите выйти из прмложения?","Окно выхода",MessageBoxButtons.OKCancel,MessageBoxIcon.Warning);
            if(rez == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult rez = MessageBox.Show("Хотите выйти из прмложения?", "Окно выхода", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (rez == DialogResult.OK)
            {
                Application.Exit();
            }
        }
        
        private void addDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddData ob = new AddData();
            ob.ShowDialog();
        }

        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                BinaryWriter fout = new BinaryWriter(new FileStream(FILE_NAME, FileMode.Create));
                fout.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Ошибка доступа к файлу данных!\n"
                    + err.ToString(), "Работа с файлом", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            load_data(FILE_NAME);
        }

        private void load_data(string filename)
        {
            allStudents.Rows.Clear();

            BinaryReader fout;
            
            try
            {
                fout = new BinaryReader(new FileStream(filename, FileMode.Open));
            }
            catch (IOException err)
            {
                Console.WriteLine(err.Message + "\n Ошибка при открытии входного файла! ");
                return;
            }

            while (fout.BaseStream.Position != fout.BaseStream.Length)
            {
                Student s;

                try
                {
                    s = new Student(fout.ReadString(), fout.ReadByte(), fout.ReadByte(), fout.ReadByte(), fout.ReadDouble());
                }
                catch (IOException err)
                {
                    Console.WriteLine(err.Message);
                    Console.WriteLine("Ошибка чтения файла!");
                    return;
                }

                allStudents.Rows.Add(s.FIO, s.ClassStudent, s.Age, (s.Gender == 0) ? "м" : "ж", s.AvgMark);
            }

            fout.Close();
        }

        private void requestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Request ob = new Request();
            ob.ShowDialog();
        }

        private void queryRequestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            load_data(FILE_NAME2);
        }

        private void aboutProgramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About ob = new About();
            ob.ShowDialog();
        }

        private void обАвтореToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Author ob = new Author();
            ob.ShowDialog();
        }
    }
}
